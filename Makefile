DESTDIR=
PREFIX=/usr/local/

install:
	install -Dm755 v $(DESTDIR)$(PREFIX)/bin
	install -Dm644 v.1 $(DESTDIR)$(PREFIX)/man/man1

.PHONY: install
