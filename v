#!/bin/sh
commit()
{
  ls -a | fgrep -xv -e . -e .. -e .v | xargs tar cjf "$dir/$(date +%s)"
  cat >>"$dir/l"
  printf '\n' >>"$dir/l"
}
rollback()
{
  [ -z "$1" ] && v=1 || v="$1"
  tar xjf "$dir/$(ls -t $dir | fgrep -xv l | sed -n \"$v\"p )"
}
nuke()
{
  ls -a | fgrep -xv -e . -e .. -e .v | xargs rm -rf --
  rollback "$1"
}
branch()
{
  [ -z "$1" ] && exit 1
  mkdir -p ".v/$1"
  printf '%s' "$1" >.v/b
}
merge()
{
  [ -z "$1" ] && exit 1
  tar xjf "$1"
  commit
}
wipe()
{
  cd "$dir"
  ls -t | fgrep -xv l | sed 1d | xargs rm --
}
dir=".v/$(cat .v/b)"
t=$1
shift
$(cat <<. | grep "^$t") "$@"
commit
rollback
nuke
branch
merge
wipe
.
